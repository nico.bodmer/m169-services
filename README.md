# M169-Services Doku Nico Bodmer

## Inhaltsverzeichnis:

<!-- TOC -->

- [1. Einleitung](#1-einleitung)
- [2. Tag 1](#2-tag-1)
- [3. Tag 2](#3-tag-2)
- [4. Tag 3](#4-tag-3)
- [5. Tag 4](#5-tag-4)
- [6. Tag 5](#6-tag-5)
- [7. Tag 6-10](#7-tag-6-10)

## 1. Einleitung

In dieser Dokumentation werde ich meine Schritte, welche ich in dem Modul 169 mache, aufzeigen. Ich baue es so auf, dass ich für jeden einzelnen Tag eine kleine Dokumentation mache, die aufzeigt, was wir erarbeitet haben. Das Ziel für mich in dieser Dokumentation wird es sein nur einen kleinen Überblick zu geben und nicht an der Schönheit des Dokumentes zu feilen, da ich noch ein LB2 Dokument mache, welches für mich die priortät hat.

## 2. Tag 1

An dem ersten Tag haben wir die Lernkompetenzen, wie die Plattformen früher waren und wie sie heute sind und noch eine Umgebung aufgesetzt

![](Image1.jpg) Der Wandel von Bare-Metal zu Cloud-Native (Virtualisierung 2.0)

Mit der Anleitung von Herr Rohr haben wir dann eine funktionstüchtige Toolumgebung aufgesetzt

## 3. Tag 2

In der zweiten Woche haben wir dann Webserver deklarativ aufgesetzt

Mit dem Video von Herr Callisto wurden die einzelnen Schritte beschrieben, um eien Webserver mit Vagrant in der TBZ-Cloud aufzusetzen

![](./Images/image2.jpg)

Wenn alles funktioniert sollte es so wie auf dem Screenshot unten aussehen
![](./Images/3.jpg)

## 4. Tag 3

Heute war das Ziel für alle Schüler, dass alle Vagrant verstehen und praktisch anwenden können. Ziel ist es, Schritt für Schritt ein deklaratives Setup für eine Webserver-Testumgebung aufzubauen, die zum Entwickeln genutzt - und in kürzester Zeit ortsunabhängig und mit wenig Aufwand - nachgebaut werden kann.

Was genau ist Vagrant?

Vagrant ist eine Open-Source-Software, die Entwicklern ermöglicht, virtuelle Entwicklungsumgebungen zu erstellen und zu verwalten, unabhängig von den Unterschieden in Betriebssystemen und Hardwarekonfigurationen. Es automatisiert den Prozess der Erstellung und Konfiguration von virtuellen Maschinen und unterstützt verschiedene Virtualisierungstechnologien wie VirtualBox, VMWare und Docker.

Mit der Anleitung von Herrn Calisto habe ich dann noch den Auftrag beenden können. Zu beginn hatte ich ein wenig schwierigkeiten, jedoch kam es mit der Zeit immer besser. Ich bekam noch Hilfe von Kollegen, was mir sehr viel brachte.

## 5. Tag 4

Container

Container ändern die Art und Weise, wie wir Software entwickeln, verteilen und laufen lassen, grundlegend.
Entwickler können Software lokal bauen, die woanders genauso laufen wird – sei es ein Rack in der IT-Abteilung, der Laptop eines Anwenders oder ein Cluster in der Cloud.
Administratoren können sich auf die Netzwerke, Ressourcen und die Uptime konzentrieren und müssen weniger Zeit mit dem Konfigurieren von Umgebungen und dem Kampf mit Systemabhängigkeiten verbringen.

Merkmale

Container teilen sich Ressourcen mit dem Host-Betriebssystem
Container können im Bruchteil einer Sekunde gestartet und gestoppt werden
Anwendungen, die in Containern laufen, verursachen wenig bis gar keinen Overhead
Container sind portierbar --> Fertig mit "Aber bei mir auf dem Rechner lief es doch!"
Container sind leichtgewichtig, d.h. es können dutzende parallel betrieben werden.
Container sind "Cloud-ready"!

## 6. Tag 5

Diese Lektion haben wir mit der LB1 begonnen, welche wir mit einem neuen Konzept in angriff genommen haben. Wir konnten die Prüfung mit allen möglichen Hilfsmitteln lösen, was heisst die meisten (inkl. mir) haben Chatgpt gebraucht.

Nach der Prüfung haben wir mit der LB2 begonnen. Zu Beginn war ich ein wenig verloren was ich genau machen muss, konnte mich dann jedoch sehr schnell orientieren was ich genau zu tun habe.

## 7. Tag 6-10

An den restlichen Tagen habe ich immer an der LB2 gearbeitet und eine eigene Dokumentation über das erstellt. An zwei Tagen habe ich noch gefehlt, was mich ein wenig in Stress versetzte, doch schlussendlich konnte ich alle Sachen, welche ich zu tun hatte abliefern.
