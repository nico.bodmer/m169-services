# LB2 Doku Nico Bodmer

## Inhaltsverzeichnis:

<!-- TOC -->

- [1. Einleitung](#1-einleitung)
- [2. Ablauf](#2-ablauf)
- [3. Service Beschreibung](#3-service-beschreibung)
- [4. Reflexion](#4-reflexion)
- [5. Quellen](#5-quellen)

<!-- TOC -->

## 1. Einleitung

In dieser Dokumentation werde ich ein wenig erläutern, welche Schritte ich durchgegagen bin, wie diese gegangen sind, wo ich Probleme hatte und zum Schluss noch schreiben, was ich nächstes Mal besser machen könnte.

Der Auftrag lautet wie folgt:

Sie erstellen - auf Basis von Docker/Podman - ein selbst gewähltes «Infrastructure as Code» Projekt, indem sie einen Service oder Serverdienst automatisieren.

## 2. Ablauf

Zu Beginn muss mit dem Befehl docker pull nginx geschaut, ob nginx schon gedownloaded wurde.
![](./Images/pic1.jpg)

Mit dem Behfehl docker ps kann geschaut werden, welche Container noch am laufen sind und mit docker ps -a, welche Container gestoppt wurden.

Mit dem Befehl docker run -p 8080:90 nginx kann der Container gestartet werden, um dies zu überprüfen kann man in einen Web-Browser gehen und dort, in meinem Fall, 10.1.44.13:8090 eingeben und es wird eine Seite kommen auf welcher steht Welcome to nginx!

![](./Images/pic2.jpg)

Um diesen dann zu stoppen kann man einfach Ctrl + C drücken. In Bash sieht dass dann etwas so aus:
![](./Images/pic3.jpg)

Folgende Befehle müssen nun eingegeben werden. Diese Befehle sind dazu da, um die einen Ordner zu erstellen und in diesem Ordner dann ein .html File

![](./Images/pic4.jpg)

Nun muss eine Verbingung zum 10.1.44.13 hergestellt werden im Visual Studio Code und dort kann dann das index.html File bearbeitet werden. In dieses File wird dann der HTML-Code geschrieben. Meinen Code werde ich im Video kurz erklären.

Um diesen Container nun zu starten muss folgender Befehl eingegeben werden: docker run -d -p 8080:90 -v ~/nginx-html:/usr/share/nginx/html --name my-nginx nginx eingegeben werden. Um nun zu überprüfen ob wirklich auch alles funktioniert hat, gehen wir wieder auf den Web-Browser und geben dort
![](./Images/pic5.jpg)

## 3. Service Beschreibung

In dem Gelb umramten Feld kann einen Text eingegeben werden, welcher dann mit dem im grünnenfeld stehenden Abschicken abgesendet werden kann. Wenn dies Erfolgreich abgeschickt werden kann kommt unter dem Abschicken Knopf einen Text auf welchem es heisst: Du hast eingegeben: "Der Text der eingegeben wurde"

![](./Images/pic6.jpg)

## 4. Reflexion

In grossen und ganzen fand ich das Thema sehr spannend und ich habe auch wirklich sehr viel gelernt. Mein Problem war das Zeitmanagement. Ich lag zu Beginn sehr gut im Zeitplan, wurde dann jedoch zwei Wochen vor den Frühlingsferien krank und habe so 4 wretvolle Stunden verloren. Die ganze Arbeit konnte ich dann in den Ferien fertigschreiben und auch noch ein Video gestalten, welche mein Projekt zeigt. Das Video war ein Sonderfall, da ich in der letzten Woche des Projektes, in welcher eigentlich die Projekte vorgestellt werden sollten, in einem obligatorischen Lehrlingslager war.

## 5. Quellen

HTML Datei erstellen: https://chat.openai.com/

Den Ablauf der Nginx Intergration: https://www.youtube.com/watch?v=mgwo8fq-SkA

Bash Cheet Sheet: https://github.com/RehanSaeed/Bash-Cheat-Sheet

Credits: David Brixel und Joseph Adam waren die beiden, welche mir in dem Projekt an der einen oder anderen Stelle weiterhelfen konnten.
